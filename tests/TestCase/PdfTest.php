<?php

namespace PartnerIT\Pdf\Test\Pdf;

use PartnerIT\Pdf\Pdf;
use \PHPUnit_Framework_TestCase;

/**
 * Class PdfTest
 * @package PartnerIT\Pdf\Test\Pdf
 */
class PdfTest extends PHPUnit_Framework_TestCase
{

	/**
	 *
	 */
	public function testInit()
	{
		$pdf = new Pdf();
		$this->assertInstanceOf(
			'PartnerIT\Pdf\Pdf',
			$pdf
		);
	}

	/**
	 *
	 */
	public function testInvalidSource()
	{

		$this->setExpectedException('RuntimeException', 'Input needs to be a URL');
		$pdf = new Pdf();
		$pdf->loadSource('some string');

	}

	/**
	 *
	 */
	public function testSource()
	{
		$pdf = new Pdf();

		try {
			$return = $pdf->loadSource('file:///' . __DIR__ . '/somefile.html');
			$this->assertInstanceOf('PartnerIT\Pdf\Pdf'
				, $return);
		} catch (\RuntimeException $exception) {
			$this->fail('we failed');
		}
	}

	/**
	 *
	 */
	public function testSetMargins()
	{
		$pdf = new Pdf();
		$pdf->setMargins(0, 0, 0, 0);

	}

	/**
	 *
	 */
	public function testConvert()
	{

		$pdf = new Pdf();
		$pdf->loadSource('file:///' . __DIR__ . '/test.html');
		$result = $pdf->convert('output.pdf');
		$this->assertTrue($result);
		unlink('output.pdf');

	}

	/**
	 *
	 */
	public function testConvertFileExsists()
	{

		$this->setExpectedException('RuntimeException', 'Output file exsists');

		touch('output2.pdf');
		$pdf = new Pdf();
		$pdf->loadSource('file:///' . __DIR__ . '/test.html');
		$result = $pdf->convert('output2.pdf');

	}

	/**
	 *
	 */
	public function testConvertFails()
	{
		$pdf = new Pdf();
		$result = $pdf->convert('output3.pdf');
		$this->assertFalse($result);
		$this->assertNotEmpty($pdf->errors);

	}

}
