<?php

namespace PartnerIT\Pdf;

/**
 * Class Pdf
 * @package PartnerIT\Pdf\Pdf
 */
class Pdf
{

	/**
	 * @var array
	 */
	private $margins = [
		'left'   => '1cm',
		'right'  => '1cm',
		'top'    => '1cm',
		'bottom' => '1cm',
	];

	/**
	 * @var array
	 */
	public $errors = [];

	/**
	 * @var null
	 */
	private $sourceUrl = null;

	/**
	 * @var string
	 */
	private $paperSize = 'A4';

	/**
	 *
	 */
	public function __construct()
	{
		if (!`which /usr/local/bin/wkhtmltopdf`) {
			throw new \RuntimeException('Please install wkhtmltopdf');
		}
	}

	/**
	 * @param $source
	 * @return $this
	 */
	public function loadSource($source)
	{

		if (filter_var($source, FILTER_VALIDATE_URL)) {
			$this->sourceUrl = $source;
		} else {
			throw new \RuntimeException('Input needs to be a URL');
		}

		return $this;
	}

	/**
	 * @param $size
	 * @return $this
	 */
	public function setPaperSize($size)
	{
		return $this;

	}

	/**
	 * @param $top
	 * @param $bottom
	 * @param $left
	 * @param $right
	 * @return $this
	 */
	public function setMargins($top, $bottom, $left, $right)
	{

		$this->margins = [
			'left'   => $left,
			'right'  => $right,
			'top'    => $top,
			'bottom' => $bottom,
		];

		return $this;
	}

	/**
	 * @param $outputFile
	 * @param bool $overwriteFile
	 * @return bool
	 */
	public function convert($outputFile, $overwriteFile = false)
	{

		if (file_exists($outputFile) && !$overwriteFile) {
			throw new \RuntimeException('Output file exsists');
		}

		$marginArg = null;
		foreach ($this->margins as $margin => $value) {

			$marginArg .= ' --margin-' . $margin . ' ' . escapeshellarg($value);

		}

		$shellCmd = '/usr/local/bin/wkhtmltopdf -s ' . escapeshellarg($this->paperSize) . ' ' . $marginArg . ' ' . escapeshellarg($this->sourceUrl) . ' ' . escapeshellarg($outputFile) . ' 2>&1';
		exec($shellCmd, $output);

		if (file_exists($outputFile)) {
			return true;
		}

		$this->errors = $output;

		return false;

	}

}
