# README #

PHP wrapper for wkhtmltopdf

### Installation

Install http://wkhtmltopdf.org first.

`$ composer require partner-it/wkhtmltopdf-php:"dev-master"`

### Usage


```
#!php
<?php

$converter = new Pdf();
$converter->loadSource('http://myurl.com/test.html');
$converter->convert('outputfile.pdf');

```

### Optional

Set paper size:

`$converter->setPaperSize('A3');`

Set margins: (top, bottom, left, right)

`$converter->setMargins('1cm','1cm', '1cm', '1cm' );`

### Possible Bugs

Text rendering may be poor with older versions of Ghostscript. Install latest version to fix.

